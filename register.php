<?php      
$nameErr = "";
$emailErr = "";
$genderErr = "";
$dateErr = "";
$name = "";
$email = "";
$gender = "";
$date = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $nameErr = "Name must be entered";
    }else {
        $name = test_input($_POST["name"]);
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed";
        }
    }

    if (empty($_POST["email"])) {
        $emailErr = "Email must be entered";
    }else {
        $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Invalid email format"; 
    }
    }

    if (empty($_POST["date"])) {
        $date = "";
    }else {
        $date = test_input($_POST["date"]);
    }

    if (empty($_POST["gender"])) {
        $genderErr = "Gender must be chosen";
    }else {
        $gender = test_input($_POST["gender"]);
    }
}

function test_input($data) {
$data = trim($data);
$data = stripslashes($data);
$data = htmlspecialchars($data);
return $data;
}
?>