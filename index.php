<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="PHP Practice - Registration Form">
<meta name="keywords" content="PHP, practice, registration form, validation">
<meta name="author" content="Mladen Reljić">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Registration Form</title>
</head>
<body>

<h1>PHP Form Validation - Extra Ugly no CSS Edition</h1>

<?php include 'register.php';?>

<div id=forma>
<form method = "post" action = "<?php echo htmlspecialchars(isset($_SERVER["register.php"]));?>">
   
    Name:
    <input type = "text" name = "name">
    <span class = "error">* <?php echo $nameErr;?></span>
    <br>
    <br>
    E-mail: 
    <input type = "text" name = "email">
    <span class = "error">* <?php echo $emailErr;?></span>
    <br>
    <br>
    Date of Birth:
    <input type = "date" name = "date">
    <span class = "error"><?php echo $dateErr;?></span>
    <br>
    <br>
    Gender: 
    <input type = "radio" name = "gender" value = "male">Male
    <input type = "radio" name = "gender" value = "female">Female
    <input type = "radio" name = "gender" value = "other">Other
    <span class = "error">* <?php echo $genderErr;?></span>
    <br>
    <br>
    <input type = "submit" name = "submit" value = "Submit"> 
   
</form>
      
<?php
    echo "<h2>Your entered values are:</h2>";
    echo $name;
    echo "<br>";
    
    echo $email;
    echo "<br>";
    
    echo date("m/d/Y/l",strtotime($date));
    echo "<br>";
    
    echo $gender;
?>
</div>


</body>
</html>